package com.example.booksapplication;

/**
 * @author: lmangoua
 * date: 16/06/20
 */

import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

public class ApiUtil {

    //to remove constructor
    private ApiUtil(){}

    public static final String BASE_API_URL = "https://www.googleapis.com/books/v1/volumes";
    public static final String QUERY_PARAMETER_KEY = "q";
    public static final String KEY = "key";
    public static final String API_KEY = "AIzaSyCcHLfuraojzJ6TQ3At7cnwOlYqVQ3WlBs"; //1st you need to enable "Books API" on https://console.developers.google.com/apis/api
    public static final String TITLE = "intitle:";
    public static final String AUTHOR = "inauthor:";
    public static final String PUBLISHER = "inpublisher:";
    public static final String ISBN = "isbn:";

    //to build url
    public static URL buildUrl(String title) {

        URL url = null;
        Uri uri = Uri.parse(BASE_API_URL).buildUpon().appendQueryParameter(QUERY_PARAMETER_KEY, title).appendQueryParameter(KEY, API_KEY).build();

        try {
            url = new URL(uri.toString());
        }
        catch(Exception e) {
            e.printStackTrace();
        }

        return url;
    }

    public static URL buildUrl(String title, String author, String publisher, String isbn) {

        URL url = null;
        StringBuilder sb = new StringBuilder();

        if(!title.isEmpty()) {
            sb.append(TITLE + title + "+");
        }

        if(!author.isEmpty()) {
            sb.append(AUTHOR + author + "+");
        }

        if(!publisher.isEmpty()) {
            sb.append(PUBLISHER + publisher + "+");
        }

        if(!isbn.isEmpty()) {
            sb.append(ISBN + isbn + "+");
        }

        //to remover the "+" sign at the end
        sb.setLength(sb.length() - 1);

        String query = sb.toString();
        Uri uri = Uri.parse(BASE_API_URL).buildUpon().appendQueryParameter(QUERY_PARAMETER_KEY, query)
                .appendQueryParameter(KEY, API_KEY)
                .build();

        try {
            url = new URL(uri.toString());
        }
        catch(Exception e) {
            e.printStackTrace();
        }

        return url;
    }

    //to connect to api
    public static String getJson(URL url) throws IOException {

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        try {
            //read data
            InputStream stream = connection.getInputStream();
            Scanner scanner = new Scanner(stream);
            scanner.useDelimiter("\\A"); //to read everything

            boolean hasData = scanner.hasNext();

            if (hasData) {
                return scanner.next();
            }
            else {
                return null;
            }
        }
        catch (Exception e) {
            Log.d("[ERROR] --- ", e.toString());
            e.printStackTrace();
            return null;
        }
        finally {
            connection.disconnect();
        }
    }

    //to parse the Json result
    public static ArrayList<Book> getBooksFromJson(String json) {

        final String ID = "id";
        final String TITLE = "title";
        final String SUBTITLE = "subtitle";
        final String AUTHORS = "authors";
        final String PUBLISHER = "publisher";
        final String PUBLISHED_DATE = "publishedDate";
        final String VOLUME_INFO = "volumeInfo";
        final String ITEMS = "items";
        final String DESCRIPTION = "description";
        final String IMAGELINKS = "imageLinks";
        final String THUMBNAIL = "thumbnail";

        ArrayList<Book> books = new ArrayList<Book>();

        try {
            JSONObject jsonBooks = new JSONObject(json); //to get the Json response
            JSONArray arrayBooks = jsonBooks.getJSONArray(ITEMS); //extract Json array of Books
            int numberOfBooks = arrayBooks.length(); //numberOfBooks = number of ITEMS

            System.out.print("jsonBooks: " + jsonBooks);
            System.out.print("arrayBooks: " + arrayBooks);

            for(int i = 0; i < numberOfBooks; i++) {
                JSONObject bookJson = arrayBooks.getJSONObject(i);
                JSONObject volumeInfoJson = bookJson.getJSONObject(VOLUME_INFO);
                JSONObject imageLinksJson = null;

                if(volumeInfoJson.has(IMAGELINKS)) {
                    imageLinksJson = volumeInfoJson.getJSONObject(IMAGELINKS);
                }
                else {
                    System.out.print("[ERROR] imageLinksJson has No Image Link!!!");
                }

                System.out.print("bookJson: " + bookJson);
                System.out.print("volumeInfoJson: " + volumeInfoJson);

                int authorNum;

                try {
                    authorNum = volumeInfoJson.getJSONArray(AUTHORS).length();
                }
                catch(Exception e) {
                    authorNum = 0;
                }

                System.out.print("authorNum: " + authorNum); //Good site to download icons: materialdesignicons.com

                //store the authors in an array
                String[] authors = new String[authorNum];
                for(int j = 0; i < authorNum; i++) {
                    authors[j] = volumeInfoJson.getJSONArray(AUTHORS).get(j).toString();
//                    authors[j] = (volumeInfoJson.isNull(AUTHORS) ?"":volumeInfoJson.getJSONArray(AUTHORS).get(j).toString());
                }

                /*
                 * Note: (volumeInfoJson.isNull(SUBTITLE) ?"":volumeInfoJson.getString(SUBTITLE)) ==> if there is no SUBTITLE print "", else print the SUBTITLE name
                 */
                //to retrieve all the Book info we need
                Book book = new Book(
                        bookJson.getString(ID),
                        volumeInfoJson.getString(TITLE),
                        (volumeInfoJson.isNull(SUBTITLE) ?"":volumeInfoJson.getString(SUBTITLE)),
                        authors,
                        (volumeInfoJson.isNull(PUBLISHER) ?"":volumeInfoJson.getString(PUBLISHER)),
                        (volumeInfoJson.isNull(PUBLISHED_DATE) ?"":volumeInfoJson.getString(PUBLISHED_DATE)),
                        (volumeInfoJson.isNull(DESCRIPTION) ?"":volumeInfoJson.getString(DESCRIPTION)),
                        (imageLinksJson == null) ?"":imageLinksJson.getString(THUMBNAIL));
                //populate List of Book
                books.add(book);
            }
        }
        catch(JSONException e) {
            e.printStackTrace();
        }
        return books;
    }
}

package com.example.booksapplication;

/**
 * @author: lmangoua
 * date: 21/06/20
 * This class is to define the views and bind data
 */

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class BooksAdapter extends RecyclerView.Adapter<BooksAdapter.BookViewHolder> {

    ArrayList<Book> books;

    //constructor
    public BooksAdapter(ArrayList<Book> books) {
        this.books = books;
    }

    //onCreateViewHolder is called when the recyclerView needs a new ViewHolder
    @NonNull
    @Override
    public BookViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View itemView = LayoutInflater.from(context).inflate(R.layout.book_list_item, parent, false);
        return new BookViewHolder(itemView);
    }

    //onBindViewHolder is called to display the data
    @Override
    public void onBindViewHolder(@NonNull BookViewHolder holder, int position) {
        Book book = books.get(position);
        holder.bind(book);
    }

    @Override
    public int getItemCount() {
        return books.size();
    }

    //BookViewHolder class
    public class BookViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvTitle;
        TextView tvAuthors;
        TextView tvDate;
        TextView tvPublisher;

        //constructor (cmd + N)
        public BookViewHolder(@NonNull View itemView) {
            super(itemView);

            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvAuthors = itemView.findViewById(R.id.tvAuthors);
            tvDate = itemView.findViewById(R.id.tvPublishedDate);
            tvPublisher = itemView.findViewById(R.id.tvPublisher);
            itemView.setOnClickListener(this); //set onClick() listener
        }

        //to bind book data to text fields we've defined
        public void bind(Book book) {

            tvTitle.setText(book.title);
            tvAuthors.setText(book.authors);
            tvDate.setText(book.publisherDate);
            tvPublisher.setText(book.publisher);
        }

        @Override
        public void onClick(View view) {

            int position = getAdapterPosition(); //retrieve position of item selected
            Book selectedBook = books.get(position); //get Book clicked by user
            Intent intent = new Intent(view.getContext(), BookDetail.class);

            intent.putExtra("Book", selectedBook);
            view.getContext().startActivity(intent);
        }
    }
}
